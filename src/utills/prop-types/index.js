import PropTypes from 'prop-types';

export const drugObjPropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  package_id: PropTypes.number.isRequired,
  product_name: PropTypes.string.isRequired,
  producer: PropTypes.string.isRequired,
  pack: PropTypes.string.isRequired,
  form: PropTypes.string.isRequired
});
