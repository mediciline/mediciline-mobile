import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lightgray',
    justifyContent: 'space-between'
  },
  listContainer: {
    flex: 8,
    margin: 3
  }
});

export default styles;
