import React from 'react';
import { Text, View } from 'react-native';

import PropTypes from 'prop-types';

import Icon from '../Icon';

const Row = ({ icon, text }) => {
  return (
    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
      <Icon appearance={icon} />
      <Text>{text}</Text>
    </View>
  );
};

Row.propTypes = {
  icon: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

export default Row;
