import React from 'react';
import renderer from 'react-test-renderer';

import BoldedText from '../index';

test('BoldedText renders correctly when nothing to bold', () => {
  const tree = renderer.create(<BoldedText allText={'test'} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test('BoldedText renders correctly when something passed to bold', () => {
  const tree = renderer
    .create(<BoldedText allText={'test'} textToBold={'es'} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('BoldedText renders correctly when something no passed to bold', () => {
  const tree = renderer
    .create(<BoldedText allText={'test'} textToBold={'ese'} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('BoldedText renders correctly when empty text to bold', () => {
  const tree = renderer.create(<BoldedText allText={''} />).toJSON();
  expect(tree).toMatchSnapshot();
});
