import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';

const BoldedText = ({ allText, textToBold }) => {
  if (textToBold.length === 0) {
    return <Text>{allText}</Text>;
  }

  const regex = new RegExp(`(.*)(${textToBold})(.*)`, 'i');
  const groupedText = allText.match(regex);

  if (!groupedText) {
    return <Text>{allText}</Text>;
  }

  return (
    <>
      <Text>{groupedText[1]}</Text>
      <Text style={{ fontWeight: 'bold' }}>{groupedText[2]}</Text>
      <Text>{groupedText[3]}</Text>
    </>
  );
};

BoldedText.defaultProps = {
  textToBold: ''
};

BoldedText.propTypes = {
  allText: PropTypes.string.isRequired,
  textToBold: PropTypes.string
};

export default BoldedText;
