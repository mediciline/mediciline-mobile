import React, { useEffect, useState } from 'react';
import { TouchableNativeFeedback, View } from 'react-native';
import PropTypes from 'prop-types';
import debounce from 'lodash/debounce';
import { TextInput } from 'react-native-paper';

import ArrowBack from '../../../../../components/shapes/ArrowBack';
import Close from '../../../../../components/shapes/Close';

import styles from './styles';

const SearchDrugs = ({
  clearSearchedDrugs,
  delay,
  findDrugs,
  navigation,
  searchText,
  setSearchText,
  t
}) => {
  useEffect(() => {
    delayFindDrugs(searchText);
  }, [searchText]);

  const delayFindDrugs = debounce(searchText => {
    if (searchText.length >= 3) {
      findDrugs(searchText);
    } else {
      clearSearchedDrugs();
    }
  }, delay);

  const onChangeSearchText = newSearchText => {
    if (newSearchText !== searchText) {
      setSearchText(newSearchText);
    }
  };

  const backToMenu = () => {
    navigation.goBack();
  };

  const clearSearchText = () => {
    setSearchText('');
  };

  return (
    <View style={styles.headerContainer}>
      <TouchableNativeFeedback onPress={backToMenu} style={styles.icon}>
        <View style={styles.icon}>
          <ArrowBack color={'#FF7F50'} />
        </View>
      </TouchableNativeFeedback>
      <View style={{ flex: 8 }}>
        <View>
          <TextInput
            value={searchText}
            onChangeText={onChangeSearchText}
            style={{ backgroundColor: 'white' }}
            label={t('add-user-drug-input-label')}
            placeholder={t('add-user-drug-input-placeholder')}
            autoFocus={true}
            theme={{ colors: { primary: '#FF7F50' } }}
          />
        </View>
      </View>
      <TouchableNativeFeedback onPress={clearSearchText} style={styles.icon}>
        <View style={styles.icon}>
          <Close color={'#A9A9A9'} />
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

SearchDrugs.defaultProps = {
  delay: 0
};

SearchDrugs.propTypes = {
  clearSearchedDrugs: PropTypes.func.isRequired,
  delay: PropTypes.number,
  findDrugs: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
  searchText: PropTypes.string.isRequired,
  setSearchText: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired
};

export default SearchDrugs;
