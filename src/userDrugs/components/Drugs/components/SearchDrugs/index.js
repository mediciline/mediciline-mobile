import { withTranslation } from 'react-i18next';

import SearchDrugs from './SearchDrugs';

export default withTranslation('common')(SearchDrugs);
