import React from 'react';

import Factory from '../../../../../components/shapes/Factory';
import Box from '../../../../../components/shapes/Box';
import Drug from '../../../../../components/shapes/Drug';

export const appearances = ['factory', 'box', 'type'];

export const icon = {
  factory: <Factory height={'16'} width={'16'} />,
  box: <Box height={'16'} width={'16'} />,
  type: <Drug height={'16'} width={'16'} />
};
