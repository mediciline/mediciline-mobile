import React from 'react';
import { View } from 'react-native';

import PropTypes from 'prop-types';

import { appearances, icon } from './config';

const Icon = ({ appearance }) => {
  return <View style={{ marginRight: 4 }}>{icon[appearance]}</View>;
};

Icon.propTypes = {
  appearance: PropTypes.oneOf(appearances).isRequired
};

export default Icon;
