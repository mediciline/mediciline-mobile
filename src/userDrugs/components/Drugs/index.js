import React, { useEffect, useState } from 'react';
import {
  FlatList,
  SafeAreaView,
  Text,
  TouchableNativeFeedback,
  View
} from 'react-native';
import PropTypes from 'prop-types';

import { drugObjPropType } from '../../../utills/prop-types';

import BoldedText from './components/BoldedText';
import Row from './components/Row';
import SearchDrugs from './components/SearchDrugs';

import styles from './styles';

const Drugs = ({ clearFoundedDrugs, delay, drugs, findDrugs, navigation }) => {
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    clearFoundedDrugs();
  }, []);

  const renderDrug = ({ item }) => {
    return (
      <TouchableNativeFeedback>
        <View style={{ marginBottom: 2, padding: 2, backgroundColor: 'white' }}>
          <Text style={{ marginLeft: 10, fontSize: 16 }}>
            <BoldedText allText={item.product_name} textToBold={searchText} />
          </Text>
          {/*<Row icon={'factory'} text={item.producer} />*/}
          <Row icon={'box'} text={item.pack} />
          <Row icon={'type'} text={item.form} />
        </View>
      </TouchableNativeFeedback>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <SearchDrugs
        clearSearchedDrugs={clearFoundedDrugs}
        delay={delay}
        findDrugs={findDrugs}
        searchText={searchText}
        setSearchText={setSearchText}
        navigation={navigation}
      />
      <View style={styles.listContainer}>
        <FlatList
          data={drugs}
          renderItem={renderDrug}
          keyExtractor={item => String(item.package_id)}
        />
      </View>
    </SafeAreaView>
  );
};

Drugs.defaultProps = {
  drugs: []
};

Drugs.propTypes = {
  clearFoundedDrugs: PropTypes.func.isRequired,
  delay: PropTypes.number,
  drugs: PropTypes.arrayOf(drugObjPropType),
  findDrugs: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired
};

export default Drugs;
