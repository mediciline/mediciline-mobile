import { connect } from 'react-redux';

import UserDrug from './UserDrugs';

const mapStateToProps = state => ({
  userDrugs: state.userDrugs
});

export default connect(mapStateToProps)(UserDrug);
