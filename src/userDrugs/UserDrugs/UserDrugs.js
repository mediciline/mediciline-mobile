import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { drugObjPropType } from '../../utills/prop-types';
import { searchDrugs } from '../../utills/drug/serachDrugs';

import Drugs from '../components/Drugs';

const UserDrug = ({ userDrugs, navigation }) => {
  const [filteredDrugs, setFilteredDrugs] = useState([...userDrugs]);

  const findDrugs = searchText => {
    setFilteredDrugs(searchDrugs(searchText, filteredDrugs));
  };

  const clearSearchedDrugs = () => {
    setFilteredDrugs([...userDrugs]);
  };

  return (
    <Drugs
      clearFoundedDrugs={clearSearchedDrugs}
      findDrugs={findDrugs}
      drugs={filteredDrugs}
      navigation={navigation}
    />
  );
};

UserDrug.propTypes = {
  userDrugs: PropTypes.arrayOf(drugObjPropType),
  navigation: PropTypes.object.isRequired
};

export default UserDrug;
