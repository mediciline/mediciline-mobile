import React from 'react';
import PropTypes from 'prop-types';

import Drugs from '../components/Drugs';

const AddUserDrug = ({ clearFetchedDrugs, drugs, fetchDrugs, navigation }) => {
  return (
    <Drugs
      clearFoundedDrugs={clearFetchedDrugs}
      delay={500}
      drugs={drugs}
      findDrugs={fetchDrugs}
      navigation={navigation}
    />
  );
};

AddUserDrug.defaultProps = {
  drugs: []
};

AddUserDrug.propTypes = {
  clearFetchedDrugs: PropTypes.func.isRequired,
  drugs: PropTypes.array,
  fetchDrugs: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired
};

export default AddUserDrug;
