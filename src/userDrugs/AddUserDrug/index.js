import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import {
  fetchDrugsRequest,
  clearFetchedDrugs
} from '../../store/actions/drugs';

import AddUserDrug from './AddUserDrug';

const mapStateToProps = state => ({
  drugs: state.drugs
});

const mapDispatchToProps = dispatch => ({
  fetchDrugs: bindActionCreators(fetchDrugsRequest, dispatch),
  clearFetchedDrugs: bindActionCreators(clearFetchedDrugs, dispatch)
});

export default withTranslation('common')(
  connect(mapStateToProps, mapDispatchToProps)(AddUserDrug)
);
