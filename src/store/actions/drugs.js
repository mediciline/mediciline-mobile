import {
  FETCH_DRUGS_REQUEST,
  FETCH_DRUGS_SUCCESS,
  CLEAR_FETCHED_DRUGS
} from '../constants/ActionTypes';

export const fetchDrugsRequest = searchText => ({
  type: FETCH_DRUGS_REQUEST,
  payload: {
    searchText
  }
});

export const fetchDrugsSuccess = drugs => ({
  type: FETCH_DRUGS_SUCCESS,
  payload: {
    drugs
  }
});

export const clearFetchedDrugs = () => ({
  type: CLEAR_FETCHED_DRUGS
});
