import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import ExpoFileSystemStorage from 'redux-persist-expo-filesystem';
import { persistStore, persistReducer } from 'redux-persist';

import createSagaMiddleware from 'redux-saga';
import reducers from '../reducer';
import rootSaga from '../sagas';

const persistConfig = {
  key: 'root',
  storage: ExpoFileSystemStorage
};

const persistedReducer = persistReducer(persistConfig, reducers);

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware, createLogger())
);

sagaMiddleware.run(rootSaga);

const persistor = persistStore(store);

export { store, persistor };
