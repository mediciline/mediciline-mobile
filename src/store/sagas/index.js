import { all } from 'redux-saga/effects';
import drugs from './drugs';

export default function* rootSaga() {
  yield all([...drugs]);
}
