import { put, takeEvery } from 'redux-saga/effects';
import { fetchDrugsSuccess } from '../actions/drugs';
import { FETCH_DRUGS_REQUEST } from '../constants/ActionTypes';
import api from '../api';

function* fetchDrugs(action) {
  const searchText = action.payload.searchText;
  const data = yield api.get('drugs').then(response => response.data);

  const filteredData = data.filter(drug =>
    drug.product_name.toLowerCase().includes(searchText.toLowerCase())
  );

  yield put(fetchDrugsSuccess(filteredData));
}

export default [takeEvery(FETCH_DRUGS_REQUEST, fetchDrugs)];
