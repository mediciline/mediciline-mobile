import {
  CLEAR_FETCHED_DRUGS,
  FETCH_DRUGS_SUCCESS
} from '../constants/ActionTypes';

const initialState = [];

export default (state = initialState, action) => {
  switch (action.type) {
    case CLEAR_FETCHED_DRUGS: {
      return [];
    }
    case FETCH_DRUGS_SUCCESS: {
      const drugs = action.payload.drugs;
      return [...drugs];
    }
    default: {
      return state;
    }
  }
};
