import { combineReducers } from 'redux';
import drugs from './drugs';
import userDrugs from './userDrugs';

export default combineReducers({
  drugs,
  userDrugs
});
