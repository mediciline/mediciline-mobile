import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Svg, { Path } from 'react-native-svg';

const ArrowBack = ({ color }) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    height="75%"
    width="75%"
    viewBox="0 0 24 24"
  >
    <Path
      d="M20,11H7.83l5.59,-5.59L12,4l-8,8 8,8 1.41,-1.41L7.83,13H20v-2z"
      fill={color}
    />
  </Svg>
);

ArrowBack.propTypes = {
  color: PropTypes.string.isRequired
};

export default memo(ArrowBack);
