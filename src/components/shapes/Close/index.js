import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Svg, { Path } from 'react-native-svg';

const Close = ({ color }) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    height="75%"
    width="75%"
    viewBox="0 0 24 24"
  >
    <Path
      d="M19,6.41L17.59,5 12,10.59 6.41,5 5,6.41 10.59,12 5,17.59 6.41,19 12,13.41 17.59,19 19,17.59 13.41,12z"
      fill={color}
    />
  </Svg>
);

Close.propTypes = {
  color: PropTypes.string.isRequired
};

export default memo(Close);
