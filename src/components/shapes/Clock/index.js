import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Svg, { Path } from 'react-native-svg';

const Clock = ({ color }) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    height="25%"
    width="25%"
    viewBox="0 0 60 60"
  >
    <Path
      d="M30,0C13.458,0 0,13.458 0,30s13.458,30 30,30s30,-13.458 30,-30S46.542,0 30,0zM30,58C14.561,58 2,45.439 2,30S14.561,2 30,2s28,12.561 28,28S45.439,58 30,58z"
      fill={color}
    />
    <Path
      d="M30,6c-0.552,0 -1,0.447 -1,1v23H14c-0.552,0 -1,0.447 -1,1s0.448,1 1,1h16c0.552,0 1,-0.447 1,-1V7C31,6.447 30.552,6 30,6z"
      fill={color}
    />
  </Svg>
);

Clock.propTypes = {
  color: PropTypes.string.isRequired
};

export default memo(Clock);
