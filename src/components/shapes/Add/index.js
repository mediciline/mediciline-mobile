import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Svg, { Path } from 'react-native-svg';

const Add = ({ color }) => (
  <Svg
    xmlns="http://www.w3.org/2000/svg"
    height="25%"
    width="25%"
    viewBox="0 0 42 42"
  >
    <Path
      d="M42,20l-20,0l0,-20l-2,0l0,20l-20,0l0,2l20,0l0,20l2,0l0,-20l20,0z"
      fill={color}
    />
  </Svg>
);

Add.propTypes = {
  color: PropTypes.string.isRequired
};

export default memo(Add);
