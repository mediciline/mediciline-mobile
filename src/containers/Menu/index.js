import { View } from 'react-native';
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import MenuItem from './components/MenuItem';

import styles from './styles';

const Menu = ({ navigation }) => {
  const addUserDrug = () => {
    navigation.navigate('AddUserDrug');
  };
  const userDrugs = () => {
    navigation.navigate('UserDrugs');
  };

  return (
    <Fragment>
      <View style={styles.body}>
        <View style={styles.row}>
          <MenuItem appearance={'addUserDrug'} openView={addUserDrug} />
          <MenuItem appearance={'scanUserDrug'} />
        </View>
        <View style={styles.row}>
          <MenuItem appearance={'usersDrugs'} openView={userDrugs} />
          <MenuItem appearance={'usedUsersDrugs'} />
        </View>
        <View style={styles.row}>
          <MenuItem appearance={'allDrugs'} />
          <MenuItem appearance={'overdueUsersDrugs'} />
        </View>
      </View>
    </Fragment>
  );
};

Menu.propTypes = {
  navigation: PropTypes.object.isRequired
};

export default Menu;
