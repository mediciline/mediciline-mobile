import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: 'black'
  },
  row: {
    flex: 1,
    flexDirection: 'row'
  }
});

export default styles;
