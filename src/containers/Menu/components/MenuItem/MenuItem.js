import { View, Text, TouchableNativeFeedback } from 'react-native';
import React from 'react';

import PropTypes from 'prop-types';

import styles from './styles';
import { appearances, backgroundColors, icon, text } from './config';

const MenuItem = ({ t, appearance, openView }) => {
  const colorStyles = {
    backgroundColor: backgroundColors[appearance]
  };

  return (
    <TouchableNativeFeedback onPress={openView} style={{ flex: 1 }}>
      <View style={[styles.item, colorStyles]}>
        {icon[appearance]}
        <Text style={styles.text}>{t(text[appearance])}</Text>
      </View>
    </TouchableNativeFeedback>
  );
};

MenuItem.defaultProps = {
  openView: () => {}
};

MenuItem.propTypes = {
  openView: PropTypes.func,
  appearance: PropTypes.oneOf(appearances).isRequired
};

export default MenuItem;
