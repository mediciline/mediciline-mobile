import React from 'react';

import Add from '../../../../components/shapes/Add';
import Scan from '../../../../components/shapes/Scan';
import FirstAid from '../../../../components/shapes/FirstAid';
import Trash from '../../../../components/shapes/Trash';
import Dictionary from '../../../../components/shapes/Dictionary';
import Clock from '../../../../components/shapes/Clock';

export const appearances = [
  'addUserDrug',
  'scanUserDrug',
  'usersDrugs',
  'usedUsersDrugs',
  'allDrugs',
  'overdueUsersDrugs'
];

export const backgroundColors = {
  addUserDrug: '#16B1E7',
  scanUserDrug: '#419CE4',
  usersDrugs: '#6A84D7',
  usedUsersDrugs: '#8B69BE',
  allDrugs: '#A04C9A',
  overdueUsersDrugs: '#A82C6F'
};

export const text = {
  addUserDrug: 'menu-add-button',
  scanUserDrug: 'menu-scan-button',
  usersDrugs: 'menu-your-button',
  usedUsersDrugs: 'menu-used-button',
  allDrugs: 'menu-dictionary-button',
  overdueUsersDrugs: 'menu-overdue-button'
};

export const icon = {
  addUserDrug: <Add color={'black'} />,
  scanUserDrug: <Scan color={'black'} />,
  usersDrugs: <FirstAid color={'black'} />,
  usedUsersDrugs: <Trash color={'black'} />,
  allDrugs: <Dictionary color={'black'} />,
  overdueUsersDrugs: <Clock color={'black'} />
};
