import { withTranslation } from 'react-i18next';

import MenuItem from './MenuItem';

export default withTranslation('common')(MenuItem);
