import React from 'react';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Menu from '../Menu';
import AddUserDrug from '../../userDrugs/AddUserDrug';
import UserDrugs from '../../userDrugs/UserDrugs';

const MainNavigator = createStackNavigator({
  Home: {
    screen: Menu,
    navigationOptions: {
      header: null
    }
  },
  AddUserDrug: {
    screen: AddUserDrug,
    navigationOptions: {
      header: null
    }
  },
  UserDrugs: {
    screen: UserDrugs,
    navigationOptions: {
      header: null
    }
  }
});

const Root = createAppContainer(MainNavigator);

export default Root;
